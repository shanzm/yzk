import request from '@/utils/request'

// 查询环境列表
export function listEnv(query) {
  return request({
    url: '/nongchanping/env/list',
    method: 'get',
    params: query
  })
}

// 查询环境详细
export function getEnv(pid) {
  return request({
    url: '/nongchanping/env/' + pid,
    method: 'get'
  })
}

// 新增环境
export function addEnv(data) {
  return request({
    url: '/nongchanping/env',
    method: 'post',
    data: data
  })
}

// 修改环境
export function updateEnv(data) {
  return request({
    url: '/nongchanping/env',
    method: 'put',
    data: data
  })
}

// 删除环境
export function delEnv(pid) {
  return request({
    url: '/nongchanping/env/' + pid,
    method: 'delete'
  })
}
