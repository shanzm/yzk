import request from '@/utils/request'

// 查询农作物列表
export function listCrops(query) {
  return request({
    url: '/nongchanping/crops/list',
    method: 'get',
    params: query
  })
}

// 查询农作物详细
export function getCrops(pid) {
  return request({
    url: '/nongchanping/crops/' + pid,
    method: 'get'
  })
}

// 新增农作物
export function addCrops(data) {
  return request({
    url: '/nongchanping/crops',
    method: 'post',
    data: data
  })
}

// 修改农作物
export function updateCrops(data) {
  return request({
    url: '/nongchanping/crops',
    method: 'put',
    data: data
  })
}

// 删除农作物
export function delCrops(pid) {
  return request({
    url: '/nongchanping/crops/' + pid,
    method: 'delete'
  })
}
