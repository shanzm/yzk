import request from '@/utils/request'

// 查询物流运输对象列表
export function listExpress(query) {
  return request({
    url: '/nongchanping/express/list',
    method: 'get',
    params: query
  })
}

// 查询物流运输对象详细
export function getExpress(pid) {
  return request({
    url: '/nongchanping/express/' + pid,
    method: 'get'
  })
}

// 新增物流运输对象
export function addExpress(data) {
  return request({
    url: '/nongchanping/express',
    method: 'post',
    data: data
  })
}

// 修改物流运输对象
export function updateExpress(data) {
  return request({
    url: '/nongchanping/express',
    method: 'put',
    data: data
  })
}

// 删除物流运输对象
export function delExpress(pid) {
  return request({
    url: '/nongchanping/express/' + pid,
    method: 'delete'
  })
}
