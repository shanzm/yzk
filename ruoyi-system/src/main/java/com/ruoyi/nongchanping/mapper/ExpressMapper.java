package com.ruoyi.nongchanping.mapper;

import java.util.List;
import com.ruoyi.nongchanping.domain.Express;

/**
 * 物流运输对象Mapper接口
 * 
 * @author 建哥
 * @date 2022-05-28
 */
public interface ExpressMapper 
{
    /**
     * 查询物流运输对象
     * 
     * @param pid 物流运输对象主键
     * @return 物流运输对象
     */
    public Express selectExpressByPid(Long pid);

    /**
     * 查询物流运输对象列表
     * 
     * @param express 物流运输对象
     * @return 物流运输对象集合
     */
    public List<Express> selectExpressList(Express express);

    /**
     * 新增物流运输对象
     * 
     * @param express 物流运输对象
     * @return 结果
     */
    public int insertExpress(Express express);

    /**
     * 修改物流运输对象
     * 
     * @param express 物流运输对象
     * @return 结果
     */
    public int updateExpress(Express express);

    /**
     * 删除物流运输对象
     * 
     * @param pid 物流运输对象主键
     * @return 结果
     */
    public int deleteExpressByPid(Long pid);

    /**
     * 批量删除物流运输对象
     * 
     * @param pids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExpressByPids(Long[] pids);
}
