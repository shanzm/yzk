package com.ruoyi.nongchanping.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.nongchanping.mapper.ExpressMapper;
import com.ruoyi.nongchanping.domain.Express;
import com.ruoyi.nongchanping.service.IExpressService;

/**
 * 物流运输对象Service业务层处理
 * 
 * @author 建哥
 * @date 2022-05-28
 */
@Service
public class ExpressServiceImpl implements IExpressService 
{
    @Autowired
    private ExpressMapper expressMapper;

    /**
     * 查询物流运输对象
     * 
     * @param pid 物流运输对象主键
     * @return 物流运输对象
     */
    @Override
    public Express selectExpressByPid(Long pid)
    {
        return expressMapper.selectExpressByPid(pid);
    }

    /**
     * 查询物流运输对象列表
     * 
     * @param express 物流运输对象
     * @return 物流运输对象
     */
    @Override
    public List<Express> selectExpressList(Express express)
    {
        return expressMapper.selectExpressList(express);
    }

    /**
     * 新增物流运输对象
     * 
     * @param express 物流运输对象
     * @return 结果
     */
    @Override
    public int insertExpress(Express express)
    {
        return expressMapper.insertExpress(express);
    }

    /**
     * 修改物流运输对象
     * 
     * @param express 物流运输对象
     * @return 结果
     */
    @Override
    public int updateExpress(Express express)
    {
        return expressMapper.updateExpress(express);
    }

    /**
     * 批量删除物流运输对象
     * 
     * @param pids 需要删除的物流运输对象主键
     * @return 结果
     */
    @Override
    public int deleteExpressByPids(Long[] pids)
    {
        return expressMapper.deleteExpressByPids(pids);
    }

    /**
     * 删除物流运输对象信息
     * 
     * @param pid 物流运输对象主键
     * @return 结果
     */
    @Override
    public int deleteExpressByPid(Long pid)
    {
        return expressMapper.deleteExpressByPid(pid);
    }
}
