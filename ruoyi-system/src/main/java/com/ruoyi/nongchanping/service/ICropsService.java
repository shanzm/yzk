package com.ruoyi.nongchanping.service;

import java.util.List;
import com.ruoyi.nongchanping.domain.Crops;

/**
 * 农作物Service接口
 * 
 * @author 建哥
 * @date 2022-05-23
 */
public interface ICropsService 
{
    /**
     * 查询农作物
     * 
     * @param pid 农作物主键
     * @return 农作物
     */
    public Crops selectCropsByPid(Long pid);

    /**
     * 查询农作物列表
     * 
     * @param crops 农作物
     * @return 农作物集合
     */
    public List<Crops> selectCropsList(Crops crops);

    /**
     * 新增农作物
     * 
     * @param crops 农作物
     * @return 结果
     */
    public int insertCrops(Crops crops);

    /**
     * 修改农作物
     * 
     * @param crops 农作物
     * @return 结果
     */
    public int updateCrops(Crops crops);

    /**
     * 批量删除农作物
     * 
     * @param pids 需要删除的农作物主键集合
     * @return 结果
     */
    public int deleteCropsByPids(Long[] pids);

    /**
     * 删除农作物信息
     * 
     * @param pid 农作物主键
     * @return 结果
     */
    public int deleteCropsByPid(Long pid);
}
