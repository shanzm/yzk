package com.ruoyi.nongchanping.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.nongchanping.mapper.CropsMapper;
import com.ruoyi.nongchanping.domain.Crops;
import com.ruoyi.nongchanping.service.ICropsService;

/**
 * 农作物Service业务层处理
 * 
 * @author 建哥
 * @date 2022-05-23
 */
@Service
public class CropsServiceImpl implements ICropsService 
{
    @Autowired
    private CropsMapper cropsMapper;

    /**
     * 查询农作物
     * 
     * @param pid 农作物主键
     * @return 农作物
     */
    @Override
    public Crops selectCropsByPid(Long pid)
    {
        return cropsMapper.selectCropsByPid(pid);
    }

    /**
     * 查询农作物列表
     * 
     * @param crops 农作物
     * @return 农作物
     */
    @Override
    public List<Crops> selectCropsList(Crops crops)
    {
        return cropsMapper.selectCropsList(crops);
    }

    /**
     * 新增农作物
     * 
     * @param crops 农作物
     * @return 结果
     */
    @Override
    public int insertCrops(Crops crops)
    {
        return cropsMapper.insertCrops(crops);
    }

    /**
     * 修改农作物
     * 
     * @param crops 农作物
     * @return 结果
     */
    @Override
    public int updateCrops(Crops crops)
    {
        return cropsMapper.updateCrops(crops);
    }

    /**
     * 批量删除农作物
     * 
     * @param pids 需要删除的农作物主键
     * @return 结果
     */
    @Override
    public int deleteCropsByPids(Long[] pids)
    {
        return cropsMapper.deleteCropsByPids(pids);
    }

    /**
     * 删除农作物信息
     * 
     * @param pid 农作物主键
     * @return 结果
     */
    @Override
    public int deleteCropsByPid(Long pid)
    {
        return cropsMapper.deleteCropsByPid(pid);
    }
}
