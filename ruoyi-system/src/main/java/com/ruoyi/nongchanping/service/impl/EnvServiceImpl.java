package com.ruoyi.nongchanping.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.nongchanping.mapper.EnvMapper;
import com.ruoyi.nongchanping.domain.Env;
import com.ruoyi.nongchanping.service.IEnvService;

/**
 * 环境Service业务层处理
 * 
 * @author 建哥
 * @date 2022-05-23
 */
@Service
public class EnvServiceImpl implements IEnvService 
{
    @Autowired
    private EnvMapper envMapper;

    /**
     * 查询环境
     * 
     * @param pid 环境主键
     * @return 环境
     */
    @Override
    public Env selectEnvByPid(Long pid)
    {
        return envMapper.selectEnvByPid(pid);
    }

    /**
     * 查询环境列表
     * 
     * @param env 环境
     * @return 环境
     */
    @Override
    public List<Env> selectEnvList(Env env)
    {
        return envMapper.selectEnvList(env);
    }

    /**
     * 新增环境
     * 
     * @param env 环境
     * @return 结果
     */
    @Override
    public int insertEnv(Env env)
    {
        return envMapper.insertEnv(env);
    }

    /**
     * 修改环境
     * 
     * @param env 环境
     * @return 结果
     */
    @Override
    public int updateEnv(Env env)
    {
        return envMapper.updateEnv(env);
    }

    /**
     * 批量删除环境
     * 
     * @param pids 需要删除的环境主键
     * @return 结果
     */
    @Override
    public int deleteEnvByPids(Long[] pids)
    {
        return envMapper.deleteEnvByPids(pids);
    }

    /**
     * 删除环境信息
     * 
     * @param pid 环境主键
     * @return 结果
     */
    @Override
    public int deleteEnvByPid(Long pid)
    {
        return envMapper.deleteEnvByPid(pid);
    }
}
