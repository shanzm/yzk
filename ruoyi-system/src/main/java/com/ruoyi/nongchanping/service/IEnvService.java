package com.ruoyi.nongchanping.service;

import java.util.List;
import com.ruoyi.nongchanping.domain.Env;

/**
 * 环境Service接口
 * 
 * @author 建哥
 * @date 2022-05-23
 */
public interface IEnvService 
{
    /**
     * 查询环境
     * 
     * @param pid 环境主键
     * @return 环境
     */
    public Env selectEnvByPid(Long pid);

    /**
     * 查询环境列表
     * 
     * @param env 环境
     * @return 环境集合
     */
    public List<Env> selectEnvList(Env env);

    /**
     * 新增环境
     * 
     * @param env 环境
     * @return 结果
     */
    public int insertEnv(Env env);

    /**
     * 修改环境
     * 
     * @param env 环境
     * @return 结果
     */
    public int updateEnv(Env env);

    /**
     * 批量删除环境
     * 
     * @param pids 需要删除的环境主键集合
     * @return 结果
     */
    public int deleteEnvByPids(Long[] pids);

    /**
     * 删除环境信息
     * 
     * @param pid 环境主键
     * @return 结果
     */
    public int deleteEnvByPid(Long pid);
}
