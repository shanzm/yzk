package com.ruoyi.nongchanping.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.xml.crypto.Data;

/**
 * 农作物对象 crops
 * 
 * @author 建哥
 * @date 2022-05-23
 */
public class Crops extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 农产品编号 */
    private Long pid;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String name;

    /** 区域编号 */
    @Excel(name = "区域编号")
    private String areacode;

    /** 亩数 */
    @Excel(name = "亩数")
    private BigDecimal acres;

    /** 种植期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "种植期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date plantingdate;

    /** 出芽期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出芽期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sprouted;

    /** 成苗期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "成苗期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date matureseed;

    /** 花期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "花期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date flowering;

    /** 结果期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结果期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date bearfruit;

    /** 采摘期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采摘期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date processdate;

    /** 保鲜期 */
    @Excel(name = "保鲜期")
    private String freshnessperiod;

    /** 图片 */
    @Excel(name = "图片")
    private String imageurl;

    private Date creatTime;


    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public void setPid(Long pid)
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setAreacode(String areacode) 
    {
        this.areacode = areacode;
    }

    public String getAreacode() 
    {
        return areacode;
    }
    public void setAcres(BigDecimal acres) 
    {
        this.acres = acres;
    }

    public BigDecimal getAcres() 
    {
        return acres;
    }
    public void setPlantingdate(Date plantingdate) 
    {
        this.plantingdate = plantingdate;
    }

    public Date getPlantingdate() 
    {
        return plantingdate;
    }
    public void setSprouted(Date sprouted) 
    {
        this.sprouted = sprouted;
    }

    public Date getSprouted() 
    {
        return sprouted;
    }
    public void setMatureseed(Date matureseed) 
    {
        this.matureseed = matureseed;
    }

    public Date getMatureseed() 
    {
        return matureseed;
    }
    public void setFlowering(Date flowering) 
    {
        this.flowering = flowering;
    }

    public Date getFlowering() 
    {
        return flowering;
    }
    public void setBearfruit(Date bearfruit) 
    {
        this.bearfruit = bearfruit;
    }

    public Date getBearfruit() 
    {
        return bearfruit;
    }
    public void setProcessdate(Date processdate) 
    {
        this.processdate = processdate;
    }

    public Date getProcessdate() 
    {
        return processdate;
    }
    public void setFreshnessperiod(String freshnessperiod) 
    {
        this.freshnessperiod = freshnessperiod;
    }

    public String getFreshnessperiod() 
    {
        return freshnessperiod;
    }
    public void setImageurl(String imageurl) 
    {
        this.imageurl = imageurl;
    }

    public String getImageurl() 
    {
        return imageurl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("pid", getPid())
            .append("name", getName())
            .append("areacode", getAreacode())
            .append("acres", getAcres())
            .append("plantingdate", getPlantingdate())
            .append("sprouted", getSprouted())
            .append("matureseed", getMatureseed())
            .append("flowering", getFlowering())
            .append("bearfruit", getBearfruit())
            .append("processdate", getProcessdate())
            .append("freshnessperiod", getFreshnessperiod())
            .append("imageurl", getImageurl())
            .toString();
    }
}
