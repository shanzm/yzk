package com.ruoyi.nongchanping.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物流运输对象对象 express
 * 
 * @author 建哥
 * @date 2022-05-28
 */
public class Express extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 物流记录id */
    private Long pid;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String product;

    /** 保鲜期 */
    @Excel(name = "保鲜期")
    private Long freshdate;

    /** 是否冷链运输(y/n) */
    @Excel(name = "是否冷链运输(y/n)")
    private String iscold;

    /** 编号 */
    @Excel(name = "编号")
    private String code;

    /** $column.columnComment */
    private Long cropsid;

    /** 运输地址 */
    @Excel(name = "运输地址")
    private String address;

    /** 日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deliverydate;

    private Crops cropInfo;
    public void setCropInfo(Crops cropInfo){
        this.cropInfo = cropInfo;
    }
    public Crops getCropInfo(){
        return  cropInfo;
    }

    public void setPid(Long pid) 
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setProduct(String product) 
    {
        this.product = product;
    }

    public String getProduct() 
    {
        return product;
    }
    public void setFreshdate(Long freshdate) 
    {
        this.freshdate = freshdate;
    }

    public Long getFreshdate() 
    {
        return freshdate;
    }
    public void setIscold(String iscold) 
    {
        this.iscold = iscold;
    }

    public String getIscold() 
    {
        return iscold;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setCropsid(Long cropsid) 
    {
        this.cropsid = cropsid;
    }

    public Long getCropsid() 
    {
        return cropsid;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setDeliverydate(Date deliverydate) 
    {
        this.deliverydate = deliverydate;
    }

    public Date getDeliverydate() 
    {
        return deliverydate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("pid", getPid())
            .append("product", getProduct())
            .append("freshdate", getFreshdate())
            .append("iscold", getIscold())
            .append("code", getCode())
            .append("cropsid", getCropsid())
            .append("address", getAddress())
            .append("deliverydate", getDeliverydate())
            .append("cropinfo", getCropInfo())
            .toString();
    }


}
