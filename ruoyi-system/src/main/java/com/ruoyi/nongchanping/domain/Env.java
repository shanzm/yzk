package com.ruoyi.nongchanping.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 环境对象 env
 * 
 * @author 建哥
 * @date 2022-05-23
 */
public class Env extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 环境记录编号 */
    private Long pid;

    /** 区域编号 */
    @Excel(name = "区域编号")
    private String areacode;

    /** 空气温度 */
    @Excel(name = "空气温度")
    private BigDecimal temp;

    /** 温度阈值 */
    @Excel(name = "温度阈值")
    private BigDecimal tempthreshold;

    /** 空气湿度 */
    @Excel(name = "空气湿度")
    private BigDecimal moisture;

    /** 湿度阈值 */
    @Excel(name = "湿度阈值")
    private BigDecimal moisturethreshold;

    /** co2浓度 */
    @Excel(name = "co2浓度")
    private BigDecimal co2;

    /** 浓度阈值 */
    @Excel(name = "浓度阈值")
    private BigDecimal co2threshold;

    /** 光照强度 */
    @Excel(name = "光照强度")
    private Long lightintensity;

    /** 光强阈值 */
    @Excel(name = "光强阈值")
    private Long lightthreshold;

    /** 土壤温度 */
    @Excel(name = "土壤温度")
    private BigDecimal soiltemp;

    /** 土温阈值 */
    @Excel(name = "土温阈值")
    private BigDecimal soiltempthreshold;

    /** 土壤湿度 */
    @Excel(name = "土壤湿度")
    private BigDecimal soilmoisture;

    /** 水分阈值 */
    @Excel(name = "水分阈值")
    private BigDecimal soilmoisturethreshold;


    private HashMap<String,Object> hashMap;

    private boolean flag;

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public HashMap getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap hashMap) {
        this.hashMap = hashMap;
    }

    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }

    private Date creattime =new Date() ;
    public void setCreattime(){
        this.creattime = new Date();
    }
    public Date getCreattime(){
        return this.creattime;
    }
    public void setPid(Long pid) 
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setAreacode(String areacode) 
    {
        this.areacode = areacode;
    }

    public String getAreacode() 
    {
        return areacode;
    }
    public void setTemp(BigDecimal temp) 
    {
        this.temp = temp;
    }

    public BigDecimal getTemp() 
    {
        return temp;
    }
    public void setTempthreshold(BigDecimal tempthreshold) 
    {
        this.tempthreshold = tempthreshold;
    }

    public BigDecimal getTempthreshold() 
    {
        return tempthreshold;
    }
    public void setMoisture(BigDecimal moisture) 
    {
        this.moisture = moisture;
    }

    public BigDecimal getMoisture() 
    {
        return moisture;
    }
    public void setMoisturethreshold(BigDecimal moisturethreshold) 
    {
        this.moisturethreshold = moisturethreshold;
    }

    public BigDecimal getMoisturethreshold() 
    {
        return moisturethreshold;
    }
    public void setCo2(BigDecimal co2) 
    {
        this.co2 = co2;
    }

    public BigDecimal getCo2() 
    {
        return co2;
    }
    public void setCo2threshold(BigDecimal co2threshold) 
    {
        this.co2threshold = co2threshold;
    }

    public BigDecimal getCo2threshold() 
    {
        return co2threshold;
    }
    public void setLightintensity(Long lightintensity) 
    {
        this.lightintensity = lightintensity;
    }

    public Long getLightintensity() 
    {
        return lightintensity;
    }
    public void setLightthreshold(Long lightthreshold) 
    {
        this.lightthreshold = lightthreshold;
    }

    public Long getLightthreshold() 
    {
        return lightthreshold;
    }
    public void setSoiltemp(BigDecimal soiltemp) 
    {
        this.soiltemp = soiltemp;
    }

    public BigDecimal getSoiltemp() 
    {
        return soiltemp;
    }
    public void setSoiltempthreshold(BigDecimal soiltempthreshold) 
    {
        this.soiltempthreshold = soiltempthreshold;
    }

    public BigDecimal getSoiltempthreshold() 
    {
        return soiltempthreshold;
    }
    public void setSoilmoisture(BigDecimal soilmoisture) 
    {
        this.soilmoisture = soilmoisture;
    }

    public BigDecimal getSoilmoisture() 
    {
        return soilmoisture;
    }
    public void setSoilmoisturethreshold(BigDecimal soilmoisturethreshold) 
    {
        this.soilmoisturethreshold = soilmoisturethreshold;
    }

    public BigDecimal getSoilmoisturethreshold() 
    {
        return soilmoisturethreshold;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("pid", getPid())
            .append("areacode", getAreacode())
            .append("temp", getTemp())
            .append("tempthreshold", getTempthreshold())
            .append("moisture", getMoisture())
            .append("moisturethreshold", getMoisturethreshold())
            .append("co2", getCo2())
            .append("co2threshold", getCo2threshold())
            .append("lightintensity", getLightintensity())
            .append("lightthreshold", getLightthreshold())
            .append("soiltemp", getSoiltemp())
            .append("soiltempthreshold", getSoiltempthreshold())
            .append("soilmoisture", getSoilmoisture())
            .append("soilmoisturethreshold", getSoilmoisturethreshold())
            .append("creattime", getCreattime())
            .toString();
    }
}
