/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : eden

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 29/05/2022 11:15:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for crops
-- ----------------------------
DROP TABLE IF EXISTS `crops`;
CREATE TABLE `crops`  (
  `pid` int NOT NULL AUTO_INCREMENT COMMENT '农产品编号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '产品名称',
  `areaCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '区域编号',
  `acres` double(255, 1) NULL DEFAULT NULL COMMENT '亩数',
  `plantingDate` date NULL DEFAULT NULL COMMENT '种植期',
  `sprouted` date NULL DEFAULT NULL COMMENT '出芽期',
  `matureSeed` date NULL DEFAULT NULL COMMENT '成苗期',
  `flowering` date NULL DEFAULT NULL COMMENT '花期',
  `bearFruit` date NULL DEFAULT NULL COMMENT '结果期',
  `processDate` date NULL DEFAULT NULL COMMENT '采摘期',
  `freshnessPeriod` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '保鲜期',
  `imageUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片',
  `creatTime` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of crops
-- ----------------------------
BEGIN;
INSERT INTO `crops` VALUES (NUll, '番茄', '1', 2.0, '2022-05-05', '2022-05-11', '2022-05-17', '2022-05-25', NULL, '2022-05-30', '5', '/profile/upload/2022/05/28/fanqie_20220528225142A001.jpeg', '2022-05-29 10:26:09');
INSERT INTO `crops` VALUES (NULL, '大蒜', '1', 1.0, '2022-04-22', '2022-04-30', '2022-05-10', NULL, NULL, '2022-05-24', NULL, '/profile/upload/2022/05/28/dasuan_20220528225225A002.jpeg', '2022-05-28 22:53:42');
INSERT INTO `crops` VALUES (NULL, '玉米', '1', 3.0, '2022-04-01', '2022-05-10', '2022-05-14', '2022-05-24', NULL, NULL, NULL, '/profile/upload/2022/05/28/yumi_20220528225900A003.jpeg', '2022-05-28 22:59:02');
INSERT INTO `crops` VALUES (NULL, '南瓜', '1', 2.0, '2022-04-01', '2022-04-11', '2022-04-17', '2022-04-25', '2022-05-03', '2022-05-27', '7', '/profile/upload/2022/05/28/nangua_20220528230549A004.jpeg', '2022-05-28 23:05:52');
INSERT INTO `crops` VALUES (NULL, '茄子', '2', 1.0, '2022-04-23', '2022-04-28', '2022-05-01', '2022-05-09', '2022-05-13', '2022-05-23', '6', '/profile/upload/2022/05/28/qiezi_20220528231921A005.jpeg', '2022-05-28 23:19:24');
INSERT INTO `crops` VALUES (NULL, '包菜', '2', 2.0, '2022-05-01', '2022-05-06', '2022-05-12', NULL, NULL, '2022-05-27', '4', '/profile/upload/2022/05/28/baocai_20220528232926A006.jpeg', '2022-05-28 23:29:29');
INSERT INTO `crops` VALUES (NULL, '辣椒', '2', 2.0, '2022-05-09', '2022-05-15', '2022-05-19', '2022-05-22', NULL, NULL, NULL, '/profile/upload/2022/05/28/lajiao_20220528233048A007.jpeg', '2022-05-28 23:30:51');
INSERT INTO `crops` VALUES (NULL, '红薯', '1', 2.0, '2022-05-01', '2022-05-08', '2022-05-12', NULL, NULL, NULL, NULL, '/profile/upload/2022/05/29/hognshu_20220529000442A008.jpeg', '2022-05-29 00:04:44');
INSERT INTO `crops` VALUES (NULL, '豌豆', '2', 1.0, '2022-05-10', '2022-05-14', '2022-05-18', '2022-05-22', NULL, '2022-05-28', '3', '/profile/upload/2022/05/29/wandou_20220529000634A009.jpeg', '2022-05-29 00:07:15');
INSERT INTO `crops` VALUES (NULL, '菜花', '2', 2.0, '2022-05-01', '2022-05-10', '2022-05-18', '2022-05-26', NULL, NULL, NULL, '/profile/upload/2022/05/29/caihua_20220529000845A010.jpeg', '2022-05-29 00:08:48');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
-- ----------------------------
-- Table structure for env
-- ----------------------------
DROP TABLE IF EXISTS `env`;
CREATE TABLE `env`  (
  `pid` bigint NOT NULL AUTO_INCREMENT COMMENT '环境记录编号',
  `areaCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '区域编号',
  `temp` double(255, 1) NULL DEFAULT NULL COMMENT '空气温度',
  `tempThreshold` double(255, 1) NULL DEFAULT NULL COMMENT '温度阈值',
  `moisture` double(255, 1) NULL DEFAULT NULL COMMENT '空气湿度',
  `moistureThreshold` double(255, 1) NULL DEFAULT NULL COMMENT '湿度阈值',
  `co2` double(255, 1) NULL DEFAULT NULL COMMENT 'co2浓度',
  `co2Threshold` double(255, 1) NULL DEFAULT NULL COMMENT '浓度阈值',
  `lightIntensity` int NULL DEFAULT NULL COMMENT '光照强度',
  `lightThreshold` int NULL DEFAULT NULL COMMENT '光强阈值',
  `soilTemp` double(255, 1) NULL DEFAULT NULL COMMENT '土壤温度',
  `soilTempThreshold` double(255, 1) NULL DEFAULT NULL COMMENT '土温阈值',
  `soilMoisture` double(255, 1) NULL DEFAULT NULL COMMENT '土壤湿度',
  `soilMoistureThreshold` double(255, 1) NULL DEFAULT NULL COMMENT '水分阈值',
  `creatTime` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of env
-- ----------------------------
BEGIN;
INSERT INTO `env` VALUES (NULL, '1', 23.0, 45.0, 34.0, 54.0, 21.0, 34.0, 15, 150, 19.0, 30.0, 45.0, 50.0, '2022-05-28 20:29:04');
INSERT INTO `env` VALUES (NULL, '2', 22.0, 45.0, 32.0, 54.0, 20.0, 34.0, 20, 150, 23.0, 30.0, 40.0, 50.0, '2022-05-28 20:29:05');
INSERT INTO `env` VALUES (NULL, '1', 25.0, 45.0, 37.0, 54.0, 20.0, 34.0, 14, 150, 21.0, 30.0, 44.0, 50.0, '2022-05-28 21:29:04');
INSERT INTO `env` VALUES (NULL, '2', 19.0, 45.0, 34.0, 54.0, 23.0, 34.0, 16, 150, 23.0, 30.0, 42.0, 50.0, '2022-05-28 21:29:04');
INSERT INTO `env` VALUES (NULL, '1', 22.0, 45.0, 39.0, 54.0, 20.0, 34.0, 13, 150, 18.0, 30.0, 43.0, 50.0, '2022-05-28 22:29:04');
INSERT INTO `env` VALUES (NULL, '2', 25.0, 45.0, 38.0, 54.0, 20.0, 34.0, 16, 150, 18.0, 30.0, 53.0, 50.0, '2022-05-28 22:29:04');
INSERT INTO `env` VALUES (NULL, '1', 23.0, 45.0, 40.0, 54.0, 21.0, 34.0, 14, 150, 20.0, 30.0, 42.0, 50.0, '2022-05-28 23:29:04');
INSERT INTO `env` VALUES (NULL, '2', 23.0, 45.0, 35.0, 54.0, 19.0, 34.0, 14, 150, 24.0, 30.0, 42.0, 50.0, '2022-05-28 23:29:04');
INSERT INTO `env` VALUES (NULL, '1', 22.0, 45.0, 43.0, 54.0, 21.0, 34.0, 13, 150, 21.0, 30.0, 39.0, 50.0, '2022-05-29 00:29:04');
INSERT INTO `env` VALUES (NULL, '2', 27.0, 45.0, 39.0, 54.0, 18.0, 34.0, 15, 150, 23.0, 30.0, 51.0, 50.0, '2022-05-28 00:29:04');
INSERT INTO `env` VALUES (NULL, '1', 20.0, 45.0, 42.0, 54.0, 23.0, 34.0, 14, 150, 22.0, 30.0, 40.0, 50.0, '2022-05-29 01:29:04');
INSERT INTO `env` VALUES (NULL, '2', 19.0, 45.0, 45.0, 54.0, 24.0, 34.0, 14, 150, 22.0, 30.0, 44.0, 50.0, '2022-05-29 01:29:04');
INSERT INTO `env` VALUES (NULL, '1', 19.0, 45.0, 39.0, 54.0, 24.0, 34.0, 15, 150, 20.0, 30.0, 41.0, 50.0, '2022-05-29 02:29:04');
INSERT INTO `env` VALUES (NULL, '2', 18.0, 45.0, 42.0, 54.0, 25.0, 34.0, 15, 150, 23.0, 30.0, 41.0, 50.0, '2022-05-29 02:29:04');
INSERT INTO `env` VALUES (NULL, '1', 18.0, 45.0, 40.0, 54.0, 22.0, 34.0, 14, 150, 19.0, 30.0, 42.0, 50.0, '2022-05-29 03:29:04');
INSERT INTO `env` VALUES (NULL, '2', 17.0, 45.0, 40.0, 54.0, 23.0, 34.0, 13, 150, 15.0, 30.0, 42.0, 50.0, '2022-05-29 03:29:04');
INSERT INTO `env` VALUES (NULL, '1', 17.0, 45.0, 41.0, 54.0, 23.0, 34.0, 15, 150, 20.0, 30.0, 39.0, 50.0, '2022-05-29 04:29:04');
INSERT INTO `env` VALUES (NULL, '2', 20.0, 45.0, 39.0, 54.0, 22.0, 34.0, 17, 150, 25.0, 30.0, 49.0, 50.0, '2022-05-29 04:29:04');
INSERT INTO `env` VALUES (NULL, '1', 16.0, 45.0, 45.0, 54.0, 21.0, 34.0, 16, 150, 17.0, 30.0, 37.0, 50.0, '2022-05-29 05:29:04');
INSERT INTO `env` VALUES (NULL, '2', 21.0, 45.0, 42.0, 54.0, 21.0, 34.0, 14, 150, 27.0, 30.0, 47.0, 50.0, '2022-05-29 05:29:04');
INSERT INTO `env` VALUES (NULL, '1', 17.0, 45.0, 46.0, 54.0, 19.0, 34.0, 14, 150, 19.0, 30.0, 39.0, 50.0, '2022-05-29 06:29:04');
INSERT INTO `env` VALUES (NULL, '2', 19.0, 45.0, 44.0, 54.0, 23.0, 34.0, 13, 150, 19.0, 30.0, 49.0, 50.0, '2022-05-29 06:29:04');
INSERT INTO `env` VALUES (NULL, '1', 15.0, 45.0, 40.0, 54.0, 24.0, 34.0, 45, 150, 22.0, 30.0, 41.0, 50.0, '2022-05-29 07:29:04');
INSERT INTO `env` VALUES (NULL, '2', 19.0, 45.0, 46.0, 54.0, 19.0, 34.0, 22, 150, 22.0, 30.0, 50.0, 50.0, '2022-05-29 07:29:04');
INSERT INTO `env` VALUES (NULL, '1', 15.0, 45.0, 40.0, 54.0, 24.0, 34.0, 45, 150, 22.0, 30.0, 41.0, 50.0, '2022-05-29 08:29:04');
INSERT INTO `env` VALUES (NULL, '2', 14.0, 45.0, 43.0, 54.0, 18.0, 34.0, 25, 150, 24.0, 30.0, 51.0, 50.0, '2022-05-29 08:29:04');
INSERT INTO `env` VALUES (NULL, '1', 19.0, 45.0, 35.0, 54.0, 20.0, 34.0, 69, 150, 22.0, 30.0, 38.0, 50.0, '2022-05-29 09:29:04');
INSERT INTO `env` VALUES (NULL, '2', 15.0, 45.0, 47.0, 54.0, 22.0, 34.0, 39, 150, 19.0, 30.0, 48.0, 50.0, '2022-05-29 09:29:04');
INSERT INTO `env` VALUES (NULL, '1', 22.0, 45.0, 34.0, 54.0, 19.0, 34.0, 75, 150, 21.0, 30.0, 37.0, 50.0, '2022-05-29 10:29:04');
INSERT INTO `env` VALUES (NULL, '2', 19.0, 45.0, 45.0, 54.0, 23.0, 34.0, 79, 150, 18.0, 30.0, 47.0, 50.0, '2022-05-29 10:29:04');
INSERT INTO `env` VALUES (NULL, '1', 24.0, 45.0, 30.0, 54.0, 18.0, 34.0, 88, 150, 23.0, 30.0, 40.0, 50.0, '2022-05-29 11:29:04');
INSERT INTO `env` VALUES (NULL, '2', 20.0, 45.0, 45.0, 54.0, 21.0, 34.0, 88, 150, 21.0, 30.0, 50.0, 50.0, '2022-05-29 11:29:04');
INSERT INTO `env` VALUES (NULL, '1', 27.0, 45.0, 35.0, 54.0, 17.0, 34.0, 95, 150, 24.0, 30.0, 45.0, 50.0, '2022-05-29 12:29:04');
INSERT INTO `env` VALUES (NULL, '2', 22.0, 45.0, 46.0, 54.0, 23.0, 34.0, 100, 150, 24.0, 30.0, 45.0, 50.0, '2022-05-29 12:29:04');
INSERT INTO `env` VALUES (NULL, '1', 28.0, 45.0, 37.0, 54.0, 15.0, 34.0, 94, 150, 24.0, 30.0, 44.0, 50.0, '2022-05-29 13:29:04');
INSERT INTO `env` VALUES (NULL, '2', 25.0, 45.0, 47.0, 54.0, 20.0, 34.0, 98, 150, 22.0, 30.0, 44.0, 50.0, '2022-05-29 13:29:04');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
-- ----------------------------
-- Table structure for express
-- ----------------------------
DROP TABLE IF EXISTS `express`;
CREATE TABLE `express`  (
  `pid` bigint NOT NULL AUTO_INCREMENT COMMENT '物流记录id',
  `product` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品名称',
  `freshDate` int NULL DEFAULT NULL COMMENT '保鲜期',
  `isCold` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否冷链运输(y/n)',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '编号',
  `cropsId` int NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '运输地址',
  `deliveryDate` datetime NULL DEFAULT NULL COMMENT '日期',
  PRIMARY KEY (`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of express
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'express', '物流表', NULL, NULL, 'Express', 'crud', 'com.ruoyi.nongchanping', 'nongchanping', 'express', '物流运输对象', '建哥', '0', '/', '{}', 'admin', '2022-05-28 21:19:15', '', '2022-05-28 21:39:21', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'pid', '物流记录id', 'bigint', 'Long', 'pid', '1', '1', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-05-28 21:19:15', '', '2022-05-28 21:39:21');
INSERT INTO `gen_table_column` VALUES (2, '1', 'product', '产品名称', 'varchar(255)', 'String', 'product', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-05-28 21:19:15', '', '2022-05-28 21:39:21');
INSERT INTO `gen_table_column` VALUES (3, '1', 'freshDate', '保鲜期', 'int', 'Long', 'freshdate', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 3, 'admin', '2022-05-28 21:19:15', '', '2022-05-28 21:39:21');
INSERT INTO `gen_table_column` VALUES (4, '1', 'isCold', '是否冷链运输', 'varchar(255)', 'String', 'iscold', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', 'sys_yes_no', 4, 'admin', '2022-05-28 21:19:15', '', '2022-05-28 21:39:21');
INSERT INTO `gen_table_column` VALUES (5, '1', 'code', '编号', 'varchar(255)', 'String', 'code', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-05-28 21:19:15', '', '2022-05-28 21:39:21');
INSERT INTO `gen_table_column` VALUES (6, '1', 'cropsId', NULL, 'int', 'Long', 'cropsid', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'select', '', 6, 'admin', '2022-05-28 21:19:15', '', '2022-05-28 21:39:21');
INSERT INTO `gen_table_column` VALUES (7, '1', 'address', '运输地址', 'varchar(255)', 'String', 'address', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, 'admin', '2022-05-28 21:19:15', '', '2022-05-28 21:39:21');
INSERT INTO `gen_table_column` VALUES (8, '1', 'deliveryDate', '日期', 'datetime', 'Date', 'deliverydate', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-05-28 21:19:15', '', '2022-05-28 21:39:21');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-05-24 21:50:43', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-05-24 21:50:43', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-05-24 21:50:43', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaOnOff', 'true', 'Y', 'admin', '2022-05-24 21:50:43', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-05-24 21:50:43', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-05-24 21:50:43', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-05-24 21:50:43', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-05-24 21:50:43', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-05-24 21:50:43', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-24 21:53:41');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-26 00:57:55');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-27 20:59:14');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-28 20:36:42');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-05-28 21:19:02');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-28 21:19:05');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-29 00:16:17');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-29 11:14:33');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2001 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '1', '0', '', 'system', 'admin', '2022-05-24 21:50:42', 'admin', '2022-05-28 00:49:48', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, '', 1, 0, 'M', '1', '1', '', 'monitor', 'admin', '2022-05-24 21:50:42', 'admin', '2022-05-24 22:00:13', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', 1, 0, 'M', '0', '1', '', 'tool', 'admin', '2022-05-24 21:50:42', 'admin', '2022-05-28 00:49:43', '系统工具目录');
INSERT INTO `sys_menu` VALUES (96, '农作物面板', 0, 5, 'crops', 'panel/crops/index', '', 1, 0, 'C', '0', '1', 'panel:crops:list', 'crops', 'admin', '2022-05-24 21:56:11', 'admin', '2022-05-28 00:50:08', '作物面板');
INSERT INTO `sys_menu` VALUES (97, '环境面板', 0, 5, 'env', 'panel/env/index', '', 1, 0, 'C', '0', '1', 'panel:env:list', 'env', 'admin', '2022-05-24 21:56:11', 'admin', '2022-05-28 00:50:05', '环境面板');
INSERT INTO `sys_menu` VALUES (98, '作物记录', 0, 1, 'crops', 'nongchanping/crops/index', '', 1, 0, 'C', '0', '0', '', 'crops', 'admin', '2022-05-24 21:56:11', 'admin', '2022-05-27 21:57:59', '作物列表菜单');
INSERT INTO `sys_menu` VALUES (99, '环境监测', 0, 2, 'env', 'nongchanping/env/index', '', 1, 0, 'C', '0', '0', 'nongchanping:env:list', 'env', 'admin', '2022-05-24 21:56:11', 'admin', '2022-05-27 21:58:08', '环境监测菜单');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-05-24 21:50:42', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-05-24 21:50:42', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-05-24 21:50:42', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '1', 'system:dept:list', 'tree', 'admin', '2022-05-24 21:50:42', 'admin', '2022-05-24 22:14:19', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '1', 'system:post:list', 'post', 'admin', '2022-05-24 21:50:42', 'admin', '2022-05-24 22:14:23', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-05-24 21:50:42', 'admin', '2022-05-24 22:14:29', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-05-24 21:50:42', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '1', 'system:notice:list', 'message', 'admin', '2022-05-24 21:50:42', 'admin', '2022-05-24 22:14:38', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-05-24 21:50:42', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-05-24 21:50:42', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-05-24 21:50:42', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-05-24 21:50:42', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-05-24 21:50:42', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-05-24 21:50:42', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-05-24 21:50:42', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-05-24 21:50:42', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-05-24 21:50:42', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-05-24 21:50:42', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-05-24 21:50:42', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '物流记录', 0, 1, 'express', 'nongchanping/express/index', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-05-24 22:01:06', 'admin', '2022-05-27 21:58:04', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2022-05-24 21:50:44', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2022-05-24 21:50:44', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 184 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"tool\",\"orderNum\":3,\"menuName\":\"系统工具\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"tool\",\"children\":[],\"createTime\":1653400242000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":3,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:00:04');
INSERT INTO `sys_oper_log` VALUES (101, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"monitor\",\"orderNum\":2,\"menuName\":\"系统监控\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"monitor\",\"children\":[],\"createTime\":1653400242000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:00:13');
INSERT INTO `sys_oper_log` VALUES (102, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"作物记录1\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"1\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:01:06');
INSERT INTO `sys_oper_log` VALUES (103, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":1,\"menuName\":\"作物记录1\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"1\",\"component\":\"nongchanping/crops/index\",\"children\":[],\"createTime\":1653400866000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:01:18');
INSERT INTO `sys_oper_log` VALUES (104, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"crops\",\"orderNum\":1,\"menuName\":\"作物记录\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"crops\",\"component\":\"nongchanping/crops/index\",\"children\":[],\"createTime\":1653400571000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":98,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:04:44');
INSERT INTO `sys_oper_log` VALUES (105, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"monitor\",\"orderNum\":4,\"menuName\":\"农产品管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"nongchanping\",\"children\":[],\"createTime\":1653400664000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":4,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:11:45');
INSERT INTO `sys_oper_log` VALUES (106, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tree\",\"orderNum\":4,\"menuName\":\"部门管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dept\",\"component\":\"system/dept/index\",\"children\":[],\"createTime\":1653400242000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":103,\"menuType\":\"C\",\"perms\":\"system:dept:list\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:14:19');
INSERT INTO `sys_oper_log` VALUES (107, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"post\",\"orderNum\":5,\"menuName\":\"岗位管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"post\",\"component\":\"system/post/index\",\"children\":[],\"createTime\":1653400242000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":104,\"menuType\":\"C\",\"perms\":\"system:post:list\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:14:23');
INSERT INTO `sys_oper_log` VALUES (108, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"dict\",\"orderNum\":6,\"menuName\":\"字典管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dict\",\"component\":\"system/dict/index\",\"children\":[],\"createTime\":1653400242000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":105,\"menuType\":\"C\",\"perms\":\"system:dict:list\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:14:26');
INSERT INTO `sys_oper_log` VALUES (109, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"dict\",\"orderNum\":6,\"menuName\":\"字典管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dict\",\"component\":\"system/dict/index\",\"children\":[],\"createTime\":1653400242000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":105,\"menuType\":\"C\",\"perms\":\"system:dict:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:14:29');
INSERT INTO `sys_oper_log` VALUES (110, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"message\",\"orderNum\":8,\"menuName\":\"通知公告\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"notice\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1653400242000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":107,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:14:38');
INSERT INTO `sys_oper_log` VALUES (111, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":1,\"menuName\":\"物流记录\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"1\",\"component\":\"nongchanping/express/index\",\"children\":[],\"createTime\":1653400866000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:23:50');
INSERT INTO `sys_oper_log` VALUES (112, '物流运输', 1, 'com.ruoyi.web.controller.nongchanping.ExpressController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"code\":\"21\",\"freshdate\":321,\"pid\":2,\"params\":{},\"iscold\":\"321\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:24:20');
INSERT INTO `sys_oper_log` VALUES (113, '物流运输', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"code\":\"21\",\"freshdate\":321,\"pid\":2,\"params\":{},\"iscold\":\"32132\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:24:23');
INSERT INTO `sys_oper_log` VALUES (114, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"crops\",\"orderNum\":5,\"menuName\":\"农作物面板\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"crops\",\"component\":\"panel/crops/index\",\"children\":[],\"createTime\":1653400571000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":96,\"menuType\":\"C\",\"perms\":\"panel:crops:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:25:26');
INSERT INTO `sys_oper_log` VALUES (115, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"env\",\"orderNum\":5,\"menuName\":\"环境面板\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"env\",\"component\":\"panel/env/index\",\"children\":[],\"createTime\":1653400571000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":97,\"menuType\":\"C\",\"perms\":\"panel:env:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:25:31');
INSERT INTO `sys_oper_log` VALUES (116, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":1,\"menuName\":\"物流记录\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"express\",\"component\":\"nongchanping/express/index\",\"children\":[],\"createTime\":1653400866000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-24 22:28:04');
INSERT INTO `sys_oper_log` VALUES (117, '农作物', 1, 'com.ruoyi.web.controller.nongchanping.CropsController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"params\":{},\"areacode\":\"423\",\"creatTime\":1653497895408,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.PersistenceException: \r\n### Error updating database.  Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String\r\n### The error may exist in file [D:\\Idea Project\\农产品生产管理系统\\RuoYi-Vue-master\\ruoyi-system\\target\\classes\\mapper\\system\\CropsMapper.xml]\r\n### The error may involve com.ruoyi.nongchanping.mapper.CropsMapper.insertCrops\r\n### The error occurred while executing an update\r\n### Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String', '2022-05-26 00:58:15');
INSERT INTO `sys_oper_log` VALUES (118, '农作物', 1, 'com.ruoyi.web.controller.nongchanping.CropsController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"params\":{},\"areacode\":\"423\",\"creatTime\":1653497958332,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.PersistenceException: \r\n### Error updating database.  Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String\r\n### The error may exist in file [D:\\Idea Project\\农产品生产管理系统\\RuoYi-Vue-master\\ruoyi-system\\target\\classes\\mapper\\system\\CropsMapper.xml]\r\n### The error may involve com.ruoyi.nongchanping.mapper.CropsMapper.insertCrops\r\n### The error occurred while executing an update\r\n### Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String', '2022-05-26 00:59:55');
INSERT INTO `sys_oper_log` VALUES (119, '农作物', 1, 'com.ruoyi.web.controller.nongchanping.CropsController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"params\":{},\"areacode\":\"423\",\"creatTime\":1653498150988,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.PersistenceException: \r\n### Error updating database.  Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String\r\n### The error may exist in file [D:\\Idea Project\\农产品生产管理系统\\RuoYi-Vue-master\\ruoyi-system\\target\\classes\\mapper\\system\\CropsMapper.xml]\r\n### The error may involve com.ruoyi.nongchanping.mapper.CropsMapper.insertCrops\r\n### The error occurred while executing an update\r\n### Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String', '2022-05-26 01:02:33');
INSERT INTO `sys_oper_log` VALUES (120, '农作物', 1, 'com.ruoyi.web.controller.nongchanping.CropsController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"params\":{},\"areacode\":\"423\",\"creatTime\":1653498157976,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.PersistenceException: \r\n### Error updating database.  Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String\r\n### The error may exist in file [D:\\Idea Project\\农产品生产管理系统\\RuoYi-Vue-master\\ruoyi-system\\target\\classes\\mapper\\system\\CropsMapper.xml]\r\n### The error may involve com.ruoyi.nongchanping.mapper.CropsMapper.insertCrops\r\n### The error occurred while executing an update\r\n### Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String', '2022-05-26 01:02:41');
INSERT INTO `sys_oper_log` VALUES (121, '农作物', 1, 'com.ruoyi.web.controller.nongchanping.CropsController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"params\":{},\"areacode\":\"423\",\"creatTime\":1653498165678,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.PersistenceException: \r\n### Error updating database.  Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String\r\n### The error may exist in file [D:\\Idea Project\\农产品生产管理系统\\RuoYi-Vue-master\\ruoyi-system\\target\\classes\\mapper\\system\\CropsMapper.xml]\r\n### The error may involve com.ruoyi.nongchanping.mapper.CropsMapper.insertCrops\r\n### The error occurred while executing an update\r\n### Cause: java.lang.IllegalArgumentException: invalid comparison: java.util.Date and java.lang.String', '2022-05-26 01:02:45');
INSERT INTO `sys_oper_log` VALUES (122, '农作物', 1, 'com.ruoyi.web.controller.nongchanping.CropsController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"params\":{},\"areacode\":\"423\",\"creatTime\":1653498221332,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'creatTime\' in \'field list\'\r\n### The error may exist in file [D:\\Idea Project\\农产品生产管理系统\\RuoYi-Vue-master\\ruoyi-system\\target\\classes\\mapper\\system\\CropsMapper.xml]\r\n### The error may involve com.ruoyi.nongchanping.mapper.CropsMapper.insertCrops-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into crops          ( name,             areaCode,             acres,                                                                                           freshnessPeriod,             imageUrl,             creatTime )           values ( ?,             ?,             ?,                                                                                           ?,             ?,             ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'creatTime\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'creatTime\' in \'field list\'', '2022-05-26 01:03:41');
INSERT INTO `sys_oper_log` VALUES (123, '农作物', 1, 'com.ruoyi.web.controller.nongchanping.CropsController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"params\":{},\"areacode\":\"423\",\"creatTime\":1653498259155,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-26 01:04:19');
INSERT INTO `sys_oper_log` VALUES (124, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"pid\":2,\"params\":{},\"areacode\":\"423\",\"plantingdate\":1650988800000,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-26 01:04:30');
INSERT INTO `sys_oper_log` VALUES (125, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"crops\",\"orderNum\":1,\"menuName\":\"作物记录\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"crops\",\"component\":\"nongchanping/crops/index\",\"children\":[],\"createTime\":1653400571000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":98,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 21:57:59');
INSERT INTO `sys_oper_log` VALUES (126, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":1,\"menuName\":\"物流记录\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"express\",\"component\":\"nongchanping/express/index\",\"children\":[],\"createTime\":1653400866000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 21:58:04');
INSERT INTO `sys_oper_log` VALUES (127, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"env\",\"orderNum\":2,\"menuName\":\"环境监测\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"env\",\"component\":\"nongchanping/env/index\",\"children\":[],\"createTime\":1653400571000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":99,\"menuType\":\"C\",\"perms\":\"nongchanping:env:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 21:58:08');
INSERT INTO `sys_oper_log` VALUES (128, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"bearfruit\":1653235200000,\"processdate\":1651334400000,\"sprouted\":1653321600000,\"pid\":2,\"flowering\":1653840000000,\"params\":{},\"areacode\":\"423\",\"plantingdate\":1650988800000,\"creatTime\":1653498259000,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\",\"matureseed\":1653235200000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 22:12:41');
INSERT INTO `sys_oper_log` VALUES (129, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"bearfruit\":1653235200000,\"processdate\":1651334400000,\"sprouted\":1653321600000,\"pid\":2,\"flowering\":1653840000000,\"params\":{},\"areacode\":\"1\",\"plantingdate\":1650988800000,\"creatTime\":1653498259000,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"423\",\"acres\":423,\"freshnessperiod\":\"423\",\"matureseed\":1653235200000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 22:12:57');
INSERT INTO `sys_oper_log` VALUES (130, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"bearfruit\":1653235200000,\"processdate\":1651334400000,\"sprouted\":1653321600000,\"pid\":2,\"flowering\":1653840000000,\"params\":{},\"areacode\":\"1\",\"plantingdate\":1650988800000,\"creatTime\":1653498259000,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"苹果\",\"acres\":423,\"freshnessperiod\":\"423\",\"matureseed\":1653235200000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 22:13:04');
INSERT INTO `sys_oper_log` VALUES (131, '环境', 2, 'com.ruoyi.web.controller.nongchanping.EnvController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/env', '127.0.0.1', '内网IP', '{\"temp\":321,\"lightintensity\":23,\"co2\":3,\"soilmoisturethreshold\":213,\"pid\":2,\"moisture\":3,\"params\":{},\"areacode\":\"1\",\"soiltempthreshold\":23,\"lightthreshold\":312,\"tempthreshold\":312,\"co2threshold\":21,\"soiltemp\":31,\"moisturethreshold\":12,\"soilmoisture\":213}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 22:28:39');
INSERT INTO `sys_oper_log` VALUES (132, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"bearfruit\":1653235200000,\"processdate\":1651334400000,\"sprouted\":1653321600000,\"pid\":2,\"flowering\":1653840000000,\"params\":{},\"areacode\":\"1\",\"plantingdate\":1650988800000,\"creatTime\":1653498259000,\"imageurl\":\"/profile/upload/2022/05/26/4cebb4bb2af8d954d40902ad44b05cd_20220526005813A001.jpg\",\"name\":\"苹果\",\"acres\":1,\"freshnessperiod\":\"423\",\"matureseed\":1653235200000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 22:56:24');
INSERT INTO `sys_oper_log` VALUES (133, '环境', 2, 'com.ruoyi.web.controller.nongchanping.EnvController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/env', '127.0.0.1', '内网IP', '{\"temp\":321,\"lightintensity\":23,\"co2\":3,\"soilmoisturethreshold\":213,\"pid\":2,\"moisture\":3,\"params\":{},\"areacode\":\"1\",\"soiltempthreshold\":23,\"lightthreshold\":312,\"tempthreshold\":312,\"co2threshold\":21,\"soiltemp\":31,\"moisturethreshold\":12,\"soilmoisture\":213}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 22:56:37');
INSERT INTO `sys_oper_log` VALUES (134, '农作物', 1, 'com.ruoyi.web.controller.nongchanping.CropsController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"bearfruit\":1653235200000,\"processdate\":1652112000000,\"sprouted\":1652889600000,\"flowering\":1653321600000,\"params\":{},\"areacode\":\"2\",\"plantingdate\":1652716800000,\"creatTime\":1653664546556,\"imageurl\":\"/profile/upload/2022/05/27/2_20220527231544A001.png\",\"name\":\"西瓜\",\"acres\":2,\"freshnessperiod\":\"2\",\"matureseed\":1653840000000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 23:15:46');
INSERT INTO `sys_oper_log` VALUES (135, '环境', 1, 'com.ruoyi.web.controller.nongchanping.EnvController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/env', '127.0.0.1', '内网IP', '{\"temp\":23,\"lightintensity\":2,\"co2\":2,\"soilmoisturethreshold\":32,\"moisture\":23,\"params\":{},\"areacode\":\"2\",\"soiltempthreshold\":32,\"lightthreshold\":32,\"tempthreshold\":23,\"co2threshold\":23,\"soiltemp\":32,\"moisturethreshold\":23,\"soilmoisture\":32}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 23:16:07');
INSERT INTO `sys_oper_log` VALUES (136, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"processdate\":1651593600000,\"pid\":3,\"params\":{},\"areacode\":\"1\",\"plantingdate\":1651334400000,\"creatTime\":1653631591000,\"imageurl\":\"/profile/upload/2022/05/27/3_20220527235235A001.png\",\"name\":\"白菜\",\"acres\":2,\"matureseed\":1651507200000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 23:52:36');
INSERT INTO `sys_oper_log` VALUES (137, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"pid\":4,\"params\":{},\"areacode\":\"2\",\"plantingdate\":1651766400000,\"creatTime\":1653625375000,\"imageurl\":\"/profile/upload/2022/05/27/2_20220527235243A002.png\",\"name\":\"茄子\",\"acres\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 23:52:43');
INSERT INTO `sys_oper_log` VALUES (138, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"pid\":5,\"params\":{},\"areacode\":\"2\",\"plantingdate\":1651766400000,\"creatTime\":1653630460000,\"imageurl\":\"/profile/upload/2022/05/27/front_20220527235248A003.png\",\"name\":\"黄瓜\",\"acres\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 23:52:49');
INSERT INTO `sys_oper_log` VALUES (139, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"pid\":6,\"params\":{},\"areacode\":\"1\",\"plantingdate\":1652025600000,\"creatTime\":1653630470000,\"imageurl\":\"/profile/upload/2022/05/27/2_20220527235256A004.png\",\"name\":\"辣椒\",\"acres\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-27 23:52:57');
INSERT INTO `sys_oper_log` VALUES (140, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"processdate\":1651593600000,\"sprouted\":1652716800000,\"pid\":3,\"params\":{},\"areacode\":\"1\",\"plantingdate\":1651334400000,\"creatTime\":1653666756000,\"imageurl\":\"/profile/upload/2022/05/27/3_20220527235235A001.png\",\"name\":\"白菜\",\"acres\":2,\"matureseed\":1651507200000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 00:20:11');
INSERT INTO `sys_oper_log` VALUES (141, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"pid\":7,\"params\":{},\"areacode\":\"2\",\"plantingdate\":1652284800000,\"creatTime\":1653659119000,\"imageurl\":\"/profile/upload/2022/05/28/3_20220528003533A001.png\",\"name\":\"西红柿\",\"acres\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 00:35:34');
INSERT INTO `sys_oper_log` VALUES (142, '环境', 2, 'com.ruoyi.web.controller.nongchanping.EnvController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/env', '127.0.0.1', '内网IP', '{\"temp\":35,\"flag\":false,\"lightintensity\":34,\"co2\":34,\"creattime\":1653649422000,\"soilmoisturethreshold\":34,\"pid\":10,\"moisture\":34,\"params\":{},\"areacode\":\"2\",\"soiltempthreshold\":34,\"lightthreshold\":34,\"tempthreshold\":34,\"co2threshold\":34,\"soiltemp\":34,\"moisturethreshold\":34,\"soilmoisture\":34}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 00:36:19');
INSERT INTO `sys_oper_log` VALUES (143, '环境', 2, 'com.ruoyi.web.controller.nongchanping.EnvController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/env', '127.0.0.1', '内网IP', '{\"temp\":35,\"flag\":false,\"lightintensity\":39,\"co2\":34,\"creattime\":1653649422000,\"soilmoisturethreshold\":34,\"pid\":10,\"moisture\":34,\"params\":{},\"areacode\":\"2\",\"soiltempthreshold\":34,\"lightthreshold\":34,\"tempthreshold\":34,\"co2threshold\":34,\"soiltemp\":34,\"moisturethreshold\":34,\"soilmoisture\":34}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 00:48:21');
INSERT INTO `sys_oper_log` VALUES (144, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tool\",\"orderNum\":3,\"menuName\":\"系统工具\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"tool\",\"children\":[],\"createTime\":1653400242000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":3,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 00:49:43');
INSERT INTO `sys_oper_log` VALUES (145, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"system\",\"orderNum\":1,\"menuName\":\"系统管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"system\",\"children\":[],\"createTime\":1653400242000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 00:49:48');
INSERT INTO `sys_oper_log` VALUES (146, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"env\",\"orderNum\":5,\"menuName\":\"环境面板\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"env\",\"component\":\"panel/env/index\",\"children\":[],\"createTime\":1653400571000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":97,\"menuType\":\"C\",\"perms\":\"panel:env:list\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 00:50:05');
INSERT INTO `sys_oper_log` VALUES (147, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"crops\",\"orderNum\":5,\"menuName\":\"农作物面板\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"crops\",\"component\":\"panel/crops/index\",\"children\":[],\"createTime\":1653400571000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":96,\"menuType\":\"C\",\"perms\":\"panel:crops:list\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 00:50:08');
INSERT INTO `sys_oper_log` VALUES (148, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'express', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:19:16');
INSERT INTO `sys_oper_log` VALUES (149, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"建哥\",\"columns\":[{\"capJavaField\":\"Pid\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pid\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"物流记录id\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1653743955000,\"tableId\":1,\"pk\":true,\"columnName\":\"pid\"},{\"capJavaField\":\"Product\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"product\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品名称\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1653743955000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"product\"},{\"capJavaField\":\"Freshdate\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"freshdate\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"保鲜期\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1653743955000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"freshDate\"},{\"capJavaField\":\"Iscold\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"iscold\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"是否冷链运输(y/n)\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:22:10');
INSERT INTO `sys_oper_log` VALUES (150, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"建哥\",\"columns\":[{\"capJavaField\":\"Pid\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"pid\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"物流记录id\",\"updateTime\":1653744130000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1653743955000,\"tableId\":1,\"pk\":true,\"columnName\":\"pid\"},{\"capJavaField\":\"Product\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"product\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品名称\",\"isQuery\":\"1\",\"updateTime\":1653744130000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1653743955000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"product\"},{\"capJavaField\":\"Freshdate\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"freshdate\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"保鲜期\",\"updateTime\":1653744130000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1653743955000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"freshDate\"},{\"capJavaField\":\"Iscold\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"iscold\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"是否冷链运输(y/n)\",\"updateTime\":1653744130000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"query', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:25:26');
INSERT INTO `sys_oper_log` VALUES (151, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-05-28 21:25:34');
INSERT INTO `sys_oper_log` VALUES (152, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"312\",\"code\":\"21\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"iscold\":\"32132\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:36:13');
INSERT INTO `sys_oper_log` VALUES (153, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"312\",\"code\":\"21\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"iscold\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:38:14');
INSERT INTO `sys_oper_log` VALUES (154, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"建哥\",\"columns\":[{\"capJavaField\":\"Pid\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"pid\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"物流记录id\",\"updateTime\":1653744326000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1653743955000,\"tableId\":1,\"pk\":true,\"columnName\":\"pid\"},{\"capJavaField\":\"Product\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"product\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品名称\",\"isQuery\":\"1\",\"updateTime\":1653744326000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1653743955000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"product\"},{\"capJavaField\":\"Freshdate\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"freshdate\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"保鲜期\",\"updateTime\":1653744326000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1653743955000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"freshDate\"},{\"capJavaField\":\"Iscold\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"sys_yes_no\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"iscold\",\"htmlType\":\"select\",\"edit\":true,\"query\":false,\"columnComment\":\"是否冷链运输\",\"updateTime\":1653744326000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:39:21');
INSERT INTO `sys_oper_log` VALUES (155, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-05-28 21:39:23');
INSERT INTO `sys_oper_log` VALUES (156, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"312\",\"code\":\"21\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:44:03');
INSERT INTO `sys_oper_log` VALUES (157, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"312\",\"code\":\"21\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"iscold\":\"N\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:44:07');
INSERT INTO `sys_oper_log` VALUES (158, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"312\",\"code\":\"北京\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"iscold\":\"N\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:46:52');
INSERT INTO `sys_oper_log` VALUES (159, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"北京\",\"code\":\"2022\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"iscold\":\"N\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:47:09');
INSERT INTO `sys_oper_log` VALUES (160, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"北京\",\"code\":\"2022\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"cropsid\":3,\"iscold\":\"N\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:54:38');
INSERT INTO `sys_oper_log` VALUES (161, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"北京\",\"code\":\"2022\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"cropsid\":6,\"iscold\":\"N\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:54:43');
INSERT INTO `sys_oper_log` VALUES (162, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"北京\",\"code\":\"2022\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"cropsid\":7,\"iscold\":\"N\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 21:54:48');
INSERT INTO `sys_oper_log` VALUES (163, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"pid\":4,\"params\":{},\"areacode\":\"2\",\"plantingdate\":1651766400000,\"creatTime\":1653666763000,\"imageurl\":\"/profile/upload/2022/05/27/2_20220527235243A002.png\",\"name\":\"茄子\",\"acres\":1,\"freshnessperiod\":\"12\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:04:23');
INSERT INTO `sys_oper_log` VALUES (164, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"processdate\":1651593600000,\"sprouted\":1652716800000,\"pid\":3,\"params\":{},\"areacode\":\"1\",\"plantingdate\":1651334400000,\"creatTime\":1653668411000,\"imageurl\":\"/profile/upload/2022/05/27/3_20220527235235A001.png\",\"name\":\"白菜\",\"acres\":2,\"freshnessperiod\":\"12\",\"matureseed\":1651507200000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:04:27');
INSERT INTO `sys_oper_log` VALUES (165, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"pid\":7,\"params\":{},\"areacode\":\"2\",\"plantingdate\":1652284800000,\"creatTime\":1653669334000,\"imageurl\":\"/profile/upload/2022/05/28/3_20220528003533A001.png\",\"name\":\"西红柿\",\"acres\":1,\"freshnessperiod\":\"dd\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:04:37');
INSERT INTO `sys_oper_log` VALUES (166, '农作物', 2, 'com.ruoyi.web.controller.nongchanping.CropsController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/crops', '127.0.0.1', '内网IP', '{\"pid\":7,\"params\":{},\"areacode\":\"2\",\"plantingdate\":1652284800000,\"creatTime\":1653746677000,\"imageurl\":\"/profile/upload/2022/05/28/3_20220528003533A001.png\",\"name\":\"西红柿\",\"acres\":1,\"freshnessperiod\":\"12\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:04:52');
INSERT INTO `sys_oper_log` VALUES (167, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"北京\",\"code\":\"2022\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"cropsid\":3,\"iscold\":\"N\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:05:00');
INSERT INTO `sys_oper_log` VALUES (168, '物流运输对象', 1, 'com.ruoyi.web.controller.nongchanping.ExpressController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"12\",\"code\":\"3213\",\"params\":{},\"deliverydate\":1653926400000,\"cropsid\":3,\"iscold\":\"Y\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'product\' doesn\'t have a default value\r\n### The error may exist in file [D:\\Idea Project\\农产品生产管理系统\\RuoYi-Vue-master\\ruoyi-system\\target\\classes\\mapper\\system\\ExpressMapper.xml]\r\n### The error may involve com.ruoyi.nongchanping.mapper.ExpressMapper.insertExpress-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into express          ( isCold,             code,             cropsId,             address,             deliveryDate )           values ( ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'product\' doesn\'t have a default value\n; Field \'product\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'product\' doesn\'t have a default value', '2022-05-28 22:06:02');
INSERT INTO `sys_oper_log` VALUES (169, '物流运输对象', 1, 'com.ruoyi.web.controller.nongchanping.ExpressController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"12\",\"code\":\"3213\",\"pid\":3,\"params\":{},\"deliverydate\":1653926400000,\"cropsid\":3,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:06:52');
INSERT INTO `sys_oper_log` VALUES (170, '物流运输对象', 1, 'com.ruoyi.web.controller.nongchanping.ExpressController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"12\",\"code\":\"3213\",\"pid\":4,\"params\":{},\"deliverydate\":1653926400000,\"cropsid\":3,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:06:52');
INSERT INTO `sys_oper_log` VALUES (171, '物流运输对象', 1, 'com.ruoyi.web.controller.nongchanping.ExpressController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"12\",\"code\":\"3213\",\"pid\":5,\"params\":{},\"deliverydate\":1653926400000,\"cropsid\":3,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:06:52');
INSERT INTO `sys_oper_log` VALUES (172, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"product\":\"321\",\"address\":\"北京\",\"code\":\"2022\",\"pid\":2,\"params\":{},\"deliverydate\":1653926400000,\"freshdate\":321,\"cropsid\":4,\"iscold\":\"N\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:07:02');
INSERT INTO `sys_oper_log` VALUES (173, '物流运输对象', 1, 'com.ruoyi.web.controller.nongchanping.ExpressController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"123\",\"code\":\"3213\",\"pid\":6,\"params\":{},\"deliverydate\":1653840000000,\"cropsid\":6,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:07:09');
INSERT INTO `sys_oper_log` VALUES (174, '物流运输对象', 3, 'com.ruoyi.web.controller.nongchanping.ExpressController.remove()', 'DELETE', 1, 'admin', NULL, '/nongchanping/express/6', '127.0.0.1', '内网IP', '{pids=6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:07:13');
INSERT INTO `sys_oper_log` VALUES (175, '物流运输对象', 3, 'com.ruoyi.web.controller.nongchanping.ExpressController.remove()', 'DELETE', 1, 'admin', NULL, '/nongchanping/express/5', '127.0.0.1', '内网IP', '{pids=5}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:07:16');
INSERT INTO `sys_oper_log` VALUES (176, '物流运输对象', 3, 'com.ruoyi.web.controller.nongchanping.ExpressController.remove()', 'DELETE', 1, 'admin', NULL, '/nongchanping/express/4', '127.0.0.1', '内网IP', '{pids=4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:07:18');
INSERT INTO `sys_oper_log` VALUES (177, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"上海\",\"code\":\"3213\",\"pid\":3,\"params\":{},\"deliverydate\":1653926400000,\"cropsid\":3,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:07:25');
INSERT INTO `sys_oper_log` VALUES (178, '物流运输对象', 2, 'com.ruoyi.web.controller.nongchanping.ExpressController.edit()', 'PUT', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"上海\",\"code\":\"3213\",\"pid\":3,\"params\":{},\"deliverydate\":1653926400000,\"cropsid\":4,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:07:33');
INSERT INTO `sys_oper_log` VALUES (179, '物流运输对象', 1, 'com.ruoyi.web.controller.nongchanping.ExpressController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"上海\",\"code\":\"1653747471543\",\"pid\":7,\"params\":{},\"deliverydate\":1652716800000,\"cropsid\":3,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:17:51');
INSERT INTO `sys_oper_log` VALUES (180, '物流运输对象', 3, 'com.ruoyi.web.controller.nongchanping.ExpressController.remove()', 'DELETE', 1, 'admin', NULL, '/nongchanping/express/3', '127.0.0.1', '内网IP', '{pids=3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:17:55');
INSERT INTO `sys_oper_log` VALUES (181, '物流运输对象', 3, 'com.ruoyi.web.controller.nongchanping.ExpressController.remove()', 'DELETE', 1, 'admin', NULL, '/nongchanping/express/2', '127.0.0.1', '内网IP', '{pids=2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:17:58');
INSERT INTO `sys_oper_log` VALUES (182, '物流运输对象', 1, 'com.ruoyi.web.controller.nongchanping.ExpressController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"北京\",\"code\":\"1653747499522\",\"pid\":8,\"params\":{},\"deliverydate\":1653667200000,\"cropsid\":3,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-28 22:18:19');
INSERT INTO `sys_oper_log` VALUES (183, '物流运输对象', 1, 'com.ruoyi.web.controller.nongchanping.ExpressController.add()', 'POST', 1, 'admin', NULL, '/nongchanping/express', '127.0.0.1', '内网IP', '{\"address\":\"21\",\"code\":\"1653754652477\",\"pid\":9,\"params\":{},\"deliverydate\":1653753600000,\"cropsid\":7,\"iscold\":\"Y\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-29 00:17:32');
INSERT INTO `sys_oper_log` VALUES (184, '农作物', 3, 'com.ruoyi.web.controller.nongchanping.CropsController.remove()', 'DELETE', 1, 'admin', NULL, '/nongchanping/crops/7', '127.0.0.1', '内网IP', '{pids=7}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-29 00:17:35');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-05-24 21:50:42', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-05-24 21:50:42', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2022-05-24 21:50:42', '', NULL, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-05-29 11:14:33', 'admin', '2022-05-24 21:50:42', '', '2022-05-29 11:14:33', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-05-24 21:50:42', 'admin', '2022-05-24 21:50:42', '', NULL, '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
