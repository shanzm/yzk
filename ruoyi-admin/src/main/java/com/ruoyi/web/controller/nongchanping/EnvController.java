package com.ruoyi.web.controller.nongchanping;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.nongchanping.domain.Env;
import com.ruoyi.nongchanping.service.IEnvService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 环境Controller
 *
 * @author 建哥
 * @date 2022-05-23
 */
@RestController
@RequestMapping("/nongchanping/env")
public class EnvController extends BaseController {
    @Autowired
    private IEnvService envService;

    /**
     * 查询环境列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Env env) {
        startPage();
        List<Env> list = envService.selectEnvList(env);
        for (Env env1 : list) {
            HashMap<String, Object> hashMap = new HashMap<>();
            Boolean flag=true;
            if (env1.getCo2().compareTo(env1.getCo2threshold()) == 1) {
                hashMap.put("co2浓度超标", env1.getCo2());
                flag=false;
            }
            if (env1.getTemp().compareTo(env1.getTempthreshold()) == 1) {
                hashMap.put("气温超标", env1.getTemp());
                flag=false;
            }
            if (env1.getLightintensity().compareTo(env1.getLightthreshold()) == 1) {
                hashMap.put("光强超标", env1.getLightintensity());
                flag=false;
            }
            if (env1.getSoilmoisture().compareTo(env1.getSoilmoisturethreshold()) == 1) {
                hashMap.put("土壤水分超标", env1.getSoilmoisturethreshold());
                flag=false;
            }
            if (env1.getSoiltemp().compareTo(env1.getSoiltempthreshold()) == 1) {
                hashMap.put("土壤温度超标", env1.getSoiltemp());
                flag=false;
            }
            env1.setHashMap(hashMap);
            env1.setFlag(flag);
        }
        return getDataTable(list);
    }
    @GetMapping("/list12")
    public TableDataInfo list12(Env env){
        List<Env> list = envService.selectEnvList(env);
        for (Env env1 : list) {
            HashMap<String, Object> hashMap = new HashMap<>();
            Boolean flag=true;
            if (env1.getCo2().compareTo(env1.getCo2threshold()) == 1) {
                hashMap.put("co2浓度超标", env1.getCo2());
                flag=false;
            }
            if (env1.getTemp().compareTo(env1.getTempthreshold()) == 1) {
                hashMap.put("气温超标", env1.getTemp());
                flag=false;
            }
            if (env1.getLightintensity().compareTo(env1.getLightthreshold()) == 1) {
                hashMap.put("光强超标", env1.getLightintensity());
                flag=false;
            }
            if (env1.getSoilmoisture().compareTo(env1.getSoilmoisturethreshold()) == 1) {
                hashMap.put("土壤水分超标", env1.getSoilmoisturethreshold());
                flag=false;
            }
            if (env1.getSoiltemp().compareTo(env1.getSoiltempthreshold()) == 1) {
                hashMap.put("土壤温度超标", env1.getSoiltemp());
                flag=false;
            }
            env1.setHashMap(hashMap);
            env1.setFlag(flag);
        }
        return getDataTable(list);
    }

    @GetMapping("/detect")
    public TableDataInfo detect(Env env) {
        startPage();
        List<Env> list = envService.selectEnvList(env);
      return  null;
    }

    /**
     * 导出环境列表
     */
    @Log(title = "环境", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Env env) {
        List<Env> list = envService.selectEnvList(env);
        ExcelUtil<Env> util = new ExcelUtil<Env>(Env.class);
        util.exportExcel(response, list, "环境数据");
    }

    /**
     * 获取环境详细信息
     */
    @GetMapping(value = "/{pid}")
    public AjaxResult getInfo(@PathVariable("pid") Long pid) {
        return AjaxResult.success(envService.selectEnvByPid(pid));
    }

    /**
     * 新增环境
     */
    @Log(title = "环境", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Env env) {
        return toAjax(envService.insertEnv(env));
    }

    /**
     * 修改环境
     */
    @Log(title = "环境", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Env env) {
        return toAjax(envService.updateEnv(env));
    }

    /**
     * 删除环境
     */
    @Log(title = "环境", businessType = BusinessType.DELETE)
    @DeleteMapping("/{pids}")
    public AjaxResult remove(@PathVariable Long[] pids) {
        return toAjax(envService.deleteEnvByPids(pids));
    }
}
