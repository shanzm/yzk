package com.ruoyi.web.controller.nongchanping;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.nongchanping.domain.Express;
import com.ruoyi.nongchanping.service.IExpressService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.nongchanping.domain.Crops;
import com.ruoyi.nongchanping.service.ICropsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 农作物Controller
 * 
 * @author 建哥
 * @date 2022-05-23
 */
@RestController
@RequestMapping("/nongchanping/crops")
public class CropsController extends BaseController
{
    @Autowired
    private ICropsService cropsService;
    @Autowired
    private IExpressService expressService;

    /**
     * 查询农作物列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Crops crops)
    {
        startPage();
        List<Crops> list = cropsService.selectCropsList(crops);
        return getDataTable(list);
    }

    /**
     * 导出农作物列表
     */
    @Log(title = "农作物", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Crops crops)
    {
        List<Crops> list = cropsService.selectCropsList(crops);
        ExcelUtil<Crops> util = new ExcelUtil<Crops>(Crops.class);
        util.exportExcel(response, list, "农作物数据");
    }

    /**
     * 获取农作物详细信息
     */
    @GetMapping(value = "/{pid}")
    public AjaxResult getInfo(@PathVariable("pid") Long pid)
    {
        return AjaxResult.success(cropsService.selectCropsByPid(pid));
    }

    /**
     * 新增农作物
     */
    @Log(title = "农作物", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Crops crops)
    {
        crops.setCreatTime(new Date());
        return toAjax(cropsService.insertCrops(crops));
    }

    /**
     * 修改农作物
     */
    @Log(title = "农作物", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Crops crops)
    {
        return toAjax(cropsService.updateCrops(crops));
    }

    /**
     * 删除农作物
     */
    @Log(title = "农作物", businessType = BusinessType.DELETE)
	@DeleteMapping("/{pids}")
    public AjaxResult remove(@PathVariable Long[] pids)
    {
        return toAjax(cropsService.deleteCropsByPids(pids));
    }
}
