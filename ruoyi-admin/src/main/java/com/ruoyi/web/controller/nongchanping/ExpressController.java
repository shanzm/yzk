package com.ruoyi.web.controller.nongchanping;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.nongchanping.domain.Crops;
import com.ruoyi.nongchanping.service.ICropsService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.nongchanping.domain.Express;
import com.ruoyi.nongchanping.service.IExpressService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物流运输对象Controller
 * 
 * @author 建哥
 * @date 2022-05-28
 */
@RestController
@RequestMapping("/nongchanping/express")
public class ExpressController extends BaseController
{
    @Autowired
    private IExpressService expressService;
    @Autowired
    private ICropsService cropsService;
    /**
     * 查询物流运输对象列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Express express)
    {
        startPage();
        List<Express> list = expressService.selectExpressList(express);
        for (Express express1 : list) {
            if(express1.getCropsid()!=null){
                Crops crops = cropsService.selectCropsByPid(express1.getCropsid());
                try {
                    if(crops.getFreshnessperiod()!=null){
                        express1.setFreshdate(Long.valueOf(crops.getFreshnessperiod()));
                    }
                }catch (Exception e){

                }


                express1.setProduct(crops.getName());
            }
        }
        return getDataTable(list);
    }
    /**
     * 查询追溯农作物物流记录
     */
    @GetMapping("/getExpress")
    public TableDataInfo getexpress(Long pid)
    {
        Express express = new Express();
        express.setCropsid(pid);
        List<Express> expresses = expressService.selectExpressList(express);
        for (Express express1 : expresses) {
            if(express1.getCropsid()!=null){
                Crops crops = cropsService.selectCropsByPid(express1.getCropsid());
                try {
                    if(crops.getFreshnessperiod()!=null){
                        express1.setFreshdate(Long.valueOf(crops.getFreshnessperiod()));
                        express1.setCropInfo(crops);
                        System.out.println(crops);
                    }
                }catch (Exception e){

                }


                express1.setProduct(crops.getName());
            }
        }
        return getDataTable(expresses);
    }
    /**
     * 导出物流运输对象列表
     */
    @Log(title = "物流运输对象", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Express express)
    {
        List<Express> list = expressService.selectExpressList(express);
        ExcelUtil<Express> util = new ExcelUtil<Express>(Express.class);
        util.exportExcel(response, list, "物流运输对象数据");
    }

    /**
     * 获取物流运输对象详细信息
     */
    @GetMapping(value = "/{pid}")
    public AjaxResult getInfo(@PathVariable("pid") Long pid)
    {
        return AjaxResult.success(expressService.selectExpressByPid(pid));
    }

    /**
     * 新增物流运输对象
     */
    @Log(title = "物流运输对象", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Express express)
    {
        express.setCode(String.valueOf(new Date().getTime()));
        return toAjax(expressService.insertExpress(express));
    }

    /**
     * 修改物流运输对象
     */
    @Log(title = "物流运输对象", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Express express)
    {
        return toAjax(expressService.updateExpress(express));
    }

    /**
     * 删除物流运输对象
     */
    @Log(title = "物流运输对象", businessType = BusinessType.DELETE)
	@DeleteMapping("/{pids}")
    public AjaxResult remove(@PathVariable Long[] pids)
    {
        return toAjax(expressService.deleteExpressByPids(pids));
    }
}
